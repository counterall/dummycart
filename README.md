This Shopping Cart demo project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) template and [Material-UI](https://material-ui.com/).

## Features
### Besides the basic requirements of the test, some additional detail can be highlighted as follows:
* Shopping cart icon also displays number of products added to the cart.
* Click shopping cart icon at top-right corner to toggle the cart popup inside which products added to cart are showed in detail.
* Click "Add to cart" button to trigger the shopping cart popup displayed at top-right corner inside which products added to cart are showed.
* Product added to cart can be entirely removed regardless of the amount by clicking a trash bin icon next to the product name.
* Product amount in cart can be changed by clicking - and + icons to decrease and increase by 1 respectively, plus user can set the amount directly in the input field placed between - and + icons.
* Shopping cart displays total value with VAT at the bottom.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

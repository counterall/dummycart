import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import Badge from '@material-ui/core/Badge';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector, useDispatch } from 'react-redux';
import {
  toggleCart,
} from '../../features/cart/cartSlice';
import {
  selectProductsCountInCart,
} from '../../features/products/productsSlice';

const useStyles = makeStyles(() => ({
  header: {
    justifyContent: 'space-between',
  }
}));

export default function Headers() {
  const classes = useStyles();
  const productsCount = useSelector(selectProductsCountInCart);
  const dispatch = useDispatch();

  return (
    <AppBar position="relative">
      <Toolbar className={classes.header}>
        <Typography variant="h6" color="inherit" noWrap>
          DummyCart
        </Typography>
        <IconButton aria-label="show new cart items" color="inherit" onClick={() => {
          dispatch(toggleCart())
        }}>
          <Badge badgeContent={productsCount} color="secondary">
          <ShoppingCartOutlinedIcon />
          </Badge>
        </IconButton>
      </Toolbar>
    </AppBar>
  );
}
import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Header from './app/header/Header'
import Footer from './app/footer/Footer'
import Cart from './features/cart/Cart'
import ProductList from './features/products/ProductList'

export default function CartApp() {
  return (
    <React.Fragment>
      <CssBaseline />
      <Header />
      <Cart />
      <ProductList />
      <Footer />
    </React.Fragment>
  );
}
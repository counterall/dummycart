import React from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';

import { useSelector, useDispatch } from 'react-redux';
import {
  addToCart,
  selectProducts,
} from './productsSlice';
import {
  showCart,
} from '../cart/cartSlice';


const useStyles = makeStyles((theme) => ({
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  productGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  product: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  productContent: {
    flexGrow: 1,
  },
  addToCart: {
    justifyContent: 'center',
  },
}));

export default function ProductList() {
  const classes = useStyles();
  const products = useSelector(selectProducts);
  const dispatch = useDispatch();

  return (
    <main>
      {/* Hero unit */}
      <div className={classes.heroContent}>
      <Container maxWidth="sm">
        <Typography component="h2" variant="h4" align="center" color="textPrimary" gutterBottom>
          TUOTELISTA
        </Typography>
      </Container>
      </div>
      <Container className={classes.productGrid} maxWidth="md">
      {/* End hero unit */}
        <Grid container spacing={6}>
          {products.map(({id, title, price, inventory}, i) => (
            <Grid item key={i} xs={12} sm={6} md={4}>
              <Card className={classes.product}>
              <CardContent className={classes.productContent}>
                <Typography gutterBottom variant="h5" component="h2">
                  {title}
                </Typography>
                <Typography>
                  Hinta: {price} EUR
                </Typography>
                <Typography>
                  {inventory ? `Varastossa: ${inventory}` : "Loppunut varastosta"}
                </Typography>
              </CardContent>
              <CardActions className={classes.addToCart}>
                <Button 
                  size="large" 
                  variant="contained" 
                  color="primary"
                  onClick={() => {
                      dispatch(addToCart(id));
                      dispatch(showCart());
                    }
                  }
                  disabled={0 === inventory}
                >
                  <AddShoppingCartIcon />&nbsp;&nbsp;Lisää ostoskoriin
                </Button>
              </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>
    </main>
  );
}
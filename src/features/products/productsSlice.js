import { createSlice } from '@reduxjs/toolkit';
import { default as rawProducts} from './products.json'

export const productsSlice = createSlice({
  name: 'products',
  initialState: {
    list: rawProducts.map(p => {
      p.reserved = 0;
      return p;
    }),
  },
  reducers: {
    addToCart: (state, action) => {
      const pid = action.payload;
      const product = state.list.find((product) => product.id === pid);

      if (product) {
        if (product.inventory) {
          product.inventory -= 1;
          product.reserved += 1;
        }
      }
    },
    removeFromCart: (state, action) => {
      const pid = action.payload;
      const product = state.list.find((product) => product.id === pid);

      if (product) {
        product.inventory += 1;
        if (product.reserved > 0) {
          product.reserved -= 1;
        }
      }
    },
    removeProductRowFromCart: (state, action) => {
      const pid = action.payload;
      const product = state.list.find((product) => product.id === pid);

      if (product) {
          product.inventory += product.reserved;
          product.reserved = 0;
      }
    },
    reserveByAmount: (state, action) => {
      const {id, amount} = action.payload;
      const product = state.list.find((product) => product.id === id);

      if (product) {
        const originalStock = product.inventory + product.reserved;
        if (amount > originalStock) {
          product.inventory = 0;
          product.reserved = originalStock;
        } else {
          product.inventory = originalStock - amount;
          product.reserved = amount;
        }
      }
    }
  },
});

export const { addToCart, removeFromCart, removeProductRowFromCart, reserveByAmount } = productsSlice.actions;

export const selectProducts = state => state.products.list;
export const selectProductsInCart = state => state.products.list.filter(p => p.reserved > 0);
export const selectProductsCountInCart = state => {
  return state.products.list.reduce((t, c) => {
    return t += c.reserved
  }, 0)
};

export default productsSlice.reducer;

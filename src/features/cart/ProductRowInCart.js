import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import AddRoundedIcon from '@material-ui/icons/AddRounded';
import RemoveRoundedIcon from '@material-ui/icons/RemoveRounded';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import Typography from '@material-ui/core/Typography';
import {
  addToCart,
  removeFromCart,
  removeProductRowFromCart,
  reserveByAmount,
} from '../products/productsSlice';
import { useDispatch } from 'react-redux';

const useStyles = makeStyles(() => ({
  meta: {
    display: "flex",
    flexFlow: "row nowrap",
    justifyContent: "space-between",
    borderBottom: "1px solid whitesmoke",
    marginBottom: "10px",
    alignItems: "center",
  },
  unitPrice: {
    fontSize: "0.8em",
    color: "lightslategray"
  },
  removeProductRowBtn: {
    position: 'relative',
    top: '-2px',
    padding: '0 10px',
  },
  reservedAmount: {
    width: '30px',
    textAlign: "center",
  }
}));

export default function ProductRowInCart({order, product: {id, title, price, inventory, reserved}}) {
  const classes = useStyles();
  const dispatch = useDispatch();

  return (
    <React.Fragment>
      <Typography variant="subtitle1" component="h2" color="textPrimary" gutterBottom>
        {`${order+1}. ${title}`}
        <IconButton aria-label="totally remove a product from cart" className={classes.removeProductRowBtn} onClick={() => {
          dispatch(removeProductRowFromCart(id))
        }}>
          <DeleteForeverOutlinedIcon fontSize="small"/>
        </IconButton>
      </Typography>
      <div className={classes.meta}>
        <Typography variant="subtitle2" component="h2" color="textPrimary" gutterBottom>
          <IconButton 
            aria-label="decrease product amount in cart" 
            size="small"
            onClick={() => {
                dispatch(removeFromCart(id));
              }
            }
          >
            <RemoveRoundedIcon />
          </IconButton>
          <input className={classes.reservedAmount} value={reserved} onChange={(e) => {
            dispatch(reserveByAmount({id, amount: Number(e.target.value)}));
          }}/>
          <IconButton 
            aria-label="increase product amount in cart" 
            size="small"
            disabled={!inventory}
            onClick={() => {
                dispatch(addToCart(id));
              }
            }
          >
            <AddRoundedIcon />
          </IconButton>
          <span className={classes.unitPrice}>{`  x  ${price}`}</span>
        </Typography>
        <Typography variant="subtitle2" component="h2" color="textPrimary" gutterBottom>
          {`${Number.parseFloat(price * reserved).toFixed(2)} EUR`}
        </Typography>
      </div>
    </React.Fragment>
  );
}
import { createSlice } from '@reduxjs/toolkit';

export const cartSlice = createSlice({
  name: 'cart',
  initialState: {
    show: false
  },
  reducers: {
    toggleCart: (state) => {
       state.show = !state.show 
    },
    showCart: (state) => {
      state.show = true
   },
  },
});

export const { toggleCart, showCart } = cartSlice.actions;
export const selectShowCart = state => state.cart.show;

export default cartSlice.reducer;

import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Grow from '@material-ui/core/Grow';
import ProductRowInCart from './ProductRowInCart'
import { useSelector, useDispatch } from 'react-redux';
import {
  toggleCart,
  selectShowCart,
} from './cartSlice';
import {
  selectProductsInCart,
} from '../products/productsSlice';

const useStyles = makeStyles(() => ({
    cart: {
        width: '300px',
        position: 'fixed',
        right: '5px',
        marginTop: '5px',
        zIndex: 1,
    },
    cartActions: {
        flexFlow: 'column nowrap',
        padding: '16px',
    },
    cartValue: {
      display: 'flex',
      flexFlow: 'row nowrap',
      justifyContent: "space-between",
      width: '100%',
      marginLeft: '0 !important',
    },
    closeBtn: {
      position: "absolute",
      top: 0,
      right: 0,
    },
}));

export default function Cart() {
  const classes = useStyles();
  const showCart = useSelector(selectShowCart);
  const productsInCart = useSelector(selectProductsInCart);
  const dispatch = useDispatch();
  const totalValue = productsInCart.reduce((t, c) => {
    return t += c.price * c.reserved
  }, 0);
  const totalValueFixed = Number.parseFloat(totalValue).toFixed(2);
  const vatValueFixed = Number.parseFloat(totalValue * 0.24).toFixed(2);

  return (
    <Grow in={showCart}>
      <Card raised={true} className={classes.cart}>
        <IconButton aria-label="close cart" className={classes.closeBtn} onClick={() => {
          dispatch(toggleCart())
        }}>
          <CloseIcon fontSize="small"/>
        </IconButton>
        <CardContent>
          {productsInCart.length ? productsInCart.map((product, i) => (
            <ProductRowInCart key={i} product={product} order={i} />
          )) : "Ostoskori on tyhjä."}
        </CardContent>
        <CardActions className={classes.cartActions}>
          <Typography variant="caption" component="h2" color="textPrimary" gutterBottom className={classes.cartValue}>
            <span>Alv. 24%</span><span>{vatValueFixed} EUR</span>
          </Typography>
          <Typography variant="subtitle2" component="h2" color="textPrimary" gutterBottom className={classes.cartValue}>
            <span>Loppusumma sis. verot</span><span>{totalValueFixed} EUR</span>
          </Typography>
        </CardActions>
      </Card>
    </Grow>
  );
}
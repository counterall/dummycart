import React from 'react';
import ReactDOM from 'react-dom';
import CartApp from './CartApp';
import store from './app/store';
import { Provider } from 'react-redux';

ReactDOM.render(
  <Provider store={store}>
    <CartApp />
  </Provider>,
  document.getElementById('root')
);
